import express from 'express';
const router = express.Router();
const InfluencerController = require('../controllers/influencer.controller');
const passport = require('passport');
require('../middleware/passport')(passport);

router.get('/getInfluencers', /*passport.authenticate('jwt', {session:false}),*/ InfluencerController.find_all);
router.get('/getAllInfluencersAuth', /*passport.authenticate('jwt', {session:false}),*/ InfluencerController.filter);
router.post('/create', InfluencerController.create);

module.exports = router;
