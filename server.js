import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

// import logger from './utils/logger';
import passport from 'passport';

// db instance connection
require('./config/db');
require('dotenv').config();

const server = express();

const path = require('path');
const fs = require('fs');

const port = process.env.PORT || 9000;
server.use(cors());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(passport.initialize());

// import user routes
const userRoutes = require('./routes/user.routes');
// import influencer routes
const influencerRoutes = require('./routes/influencer.routes');

// register user routes
server.use('/api/user', userRoutes);
// register influencer routes
server.use('/api/in', influencerRoutes);

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
