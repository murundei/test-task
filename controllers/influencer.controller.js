const { to, throwError, error, success } = require('../utils/requestHelpers');
const InfluencerModel = require("../models/influencer.model");

const find_all = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const { body } = req.body;
  let err, all_influencers;
  [err, all_influencers]= await to(InfluencerModel.find({}));
  if (err) return error(res, err.message, 400);
  return success(res, all_influencers);
};

const filter = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  let err, influencers;

  const query = req.query;
  const conditions = Object.keys(query)
      .reduce((result, key) => {
        if (query[key]) {
          result[key] = query[key];
        }
        return result;
      }, {});
  console.log(conditions);
  [err, influencers] = await to(InfluencerModel.find(conditions));

  if (err) return error(res, err.message, 400);
  return success(res, influencers);
};

const create = async (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  let err, new_influencer;
  [err, new_influencer] = await to(InfluencerModel.create(req.body));
  if(err) throwError(err.message);
  return new_influencer;
};

exports.find_all = find_all;
exports.filter = filter;
exports.create = create;