const mongoose = require("mongoose");
const moment = require("moment");

/*
 * Influencer Model Schema for MongoDB
 * */
const mongooseSchema = {
  uuid: {
    type: String,
    unique: true,
    default: ""
  },

  paginate: {
    type: Boolean
  },
  skip: {
    type: Number
  },
  pageSize: {
    type: Number
  },
  search: {
    type: String
  },
  name: {
    type: String
  },
  sort: {
    type: String
  },
  sort_order: {
    type: String
  },
  locations: {
    type: Array
  },
  gender: {
    type: String
  },
  city: {
    type: String
  },
  followers_count: {
    type: String
  },
  age: {
    type: String
  },

  quality: [{
    rate: {
      type: Number
    },
    datetime: {
      type: String
    },

  }],


  instaBusinessId: {
    type: String,
    required: false,
    trim: true,
    minlength: 1
  },
  /*instagram user name*/
  instagram_handle: {
    type: String,
    // unique: true,
    //required: true,
    minlength: 1
  },


  biography: {
    type: String,
    default: "",
    required: false
  },
  about: {
    type: String,
    default: "",
    required: false
  },

  category: [{
    type: String
  }],

  stats: {
    impressions: {
      type: Number,
      default: 0.0,
      required: false
    },
    reach: {
      type: Number,
      default: 0.0,
      required: false
    },
    profile_views: {
      type: Number,
      default: 0.0,
      required: false
    },
    audience_city: [
      {
        city: String,
        audience_count: Number
      }
    ],
    audience_country: [
      {
        country: String,
        audience_count: Number
      }
    ],
    audience_gender_age: {
      male: {
        type: Number,
        default: 0,
        required: false
      },
      female: {
        type: Number,
        default: 0,
        required: false
      },
      ageStatsTotal: {
        type: Object,
        default: {},
        required: false
      },
      ageStatsGender: {
        female: {},
        male: {}
      }
    },

    media_count: {
      type: Number,
      default: 0,
      required: false
    },
    average_likes: {
      /*totalLikesonMeia Array / mediarraySize*/
      type: Number,
      default: 0,
      required: false
    },
    enagement_rate: {
      /*avgLike / totalFollowersCount*/
      type: Number,
      default: 0,
      required: false
    }
  },

  userMedia: [
    {
      like_count: Number,
      media_url: String,
      media_type: String,
      comments_count: Number,
      id: String,
      show: {
        type: Boolean,
        default: true
      }
    }
  ],

  demand: {
    value: {
      type: Number,
      default: 0.0,
      required: false
    },
    cash_value_per_post: {
      type: Number,
      default: 0.0,
      required: false
    },
    cash_value_per_story: {
      type: Number,
      default: 0.0,
      required: false
    },
    guidelines: {
      type: String
    },

    benefit_value: {
      type: Number,
      default: 0.0,
      required: false
    },

    free_stuff_included: {
      type: Boolean,
      default: true,
      required: false
    },
    free_stuff_description: {
      type: String,
      default: "",
      required: false
    },
    free_stuff_money_equivalent: {
      type: String,
      default: "",
      required: false
    },
    share: {
      no_of_posts: {
        type: Number,
        default: 0.0,
        required: false
      },
      no_of_stories: {
        type: Number,
        default: 0.0,
        required: false
      }
    },
    promotion_description: {
      type: String,
      default: "",
      required: false
    }
  },

  ratingByBusiness: {
    ratings: [
      {
        businessUuid: {
          type: String
        },

        businessId: {
          type: String
        },
        rating: {
          type: Number,
          default: 0.0
        },
        comment: {
          type: String,
          default: ""
        },
        updatedDate: {
          type: Date

        }
      }
    ],
    totalUserRatedTillNow: {
      type: Number,
      default: 0
    },
    sumOfAllRatings: {
      type: Number,
      default: 0
    }
  },
  other_accounts: {
    youtube_link: {
      type: String,
      required: false
    },
    twitter_link: {
      type: String,
      required: false
    },
    snapchat_link: {
      type: String,
      required: false
    }
  },

  favouriteList: [
    {
      type: String
    }
  ],

  blockShowInterest: {
    counter: {
      type: Number,
      default: 0
    },
    expiration_date: {
      type: Date,
      default: moment().format("YYYY-MM-DD[T]HH:mm:ss[Z]")
    }
  },

  interested_in: [
    {
      businessId: {
        type: String
      },
      message: {
        type: String
      },
      createdDate: {
        type: Date
      },
      lastNotificationDate: {
        type: Date
      },
      expiration_date: {
        type: Date
      }

    }],

  not_interested: [
    {
      businessId: {
        type: String
      },
      expiration_date: {
        type: Date
      }
    }],

  collaboration: [
    {
      businessId: {type: String},
      status: {type: String},
      previousStatus: {type: String},
      createdDate: {
        type: Date
      },
      updatedDate: {
        type: Date
      },
      lastNotificationDate: {
        type: Date
      },
      expiration_date: {
        type: Date
      }
    }
  ],

  tags: {
    type: Array
  },

  acceptance_rate: {
    type: Number,
    default: 100


  },

  lastMessageShowInterest: {
    "businessId": {type: String},
    "message": String
  }

};

const InfluencerSchema = new mongoose.Schema(mongooseSchema, {
  versionKey: false
});

const influencerModel = mongoose.model("Influencer", InfluencerSchema);
module.exports = influencerModel;